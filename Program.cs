﻿// See https://aka.ms/new-console-template for more information
using ProductApp.Model;
using ProductApp.Repository;

//Console.WriteLine("Hello, World!");
ProductRepository productRepository = new ProductRepository();
Product[] products = productRepository.GetAllProducts();
foreach (Product item in products)
{
    Console.WriteLine(item);
    //if (item.Category=="Mobile")
    //{
    //    Console.WriteLine($"Product Name: {item.Name}, Product Category: {item.Category}, Product Price: {item.Price}, Product Rating: {item.Rating}");
    //}
}
Product[] mobiles = productRepository.GetProductsByCategory("Mobile");
foreach (Product item in mobiles)
{
    Console.WriteLine(item);
}
Console.WriteLine("---------------------------");
Product productA = new Product("Nothing", "Mobile", 45000, 4.1f);
products = productRepository.UpdateProduct(products, 1, productA);
foreach (Product product in products)
{
    Console.WriteLine(product);
}
Console.WriteLine("---------------------------");
products = productRepository.DeleteProduct(products, 2);
foreach (Product product in products)
{
    Console.WriteLine(product);
}
Console.WriteLine("---------------------------");
Product[] nothingProducts = productRepository.GetProductsByName("Nothing");
foreach (Product product in nothingProducts)
{
    Console.WriteLine(product);
}
Console.WriteLine("---------------------------");
Product redmi = new Product("Redmi", "Mobile", 45000, 4.0f);
products = productRepository.AddProduct(redmi);
foreach (Product product in products)
{
    Console.WriteLine(product);
}

//foreach (Product product in products)
//{
//    product.Price += 500;
//    Console.WriteLine(product);
//}

//Product productA = new Product("Nothing", "Mobile", 45000, 4.1f);
//Console.WriteLine(productA);
