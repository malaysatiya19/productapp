﻿using ProductApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    internal class ProductRepository
    {
        Product[] products;

        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Oppo", "Mobile", 15000, 4.2f),
                new Product("Moto", "Mobile", 17000, 4.3f),
                new Product("Samsung", "TV", 25000, 4.2f),
                new Product("Lenovo", "Laptop", 45000, 4.2f)
            };
        }

        public Product[] GetAllProducts()
        {
            return products;
        }

        public Product[] AddProduct(Product product)
        {
            Array.Resize(ref products, products.Length+1);
            products[products.Length-1] = product;
            return products;
        }

        public Product[] GetProductsByCategory(string category)
        {
            return Array.FindAll(products, product => product.Category == category);
        }
        public Product[] GetProductsByName(string productName)
        {
            return Array.FindAll(products, product => product.Name == productName);
        }

        public Product[] DeleteProduct(Product[] products, int index)
        {
            return Array.FindAll(products, product => product != products[index]);
        }

        public Product[] UpdateProduct(Product[] products, int index, Product product)
        {
            products[index] = product;
            return products;
        }
    }
}
